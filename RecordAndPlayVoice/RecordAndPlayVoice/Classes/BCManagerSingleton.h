//
//  BCManagerSingleton.h
//  RecordAndPlayVoice
//
//  Created by BigCat on 2017/6/23.
//  Copyright © 2017年 liuchunlao. All rights reserved.
//  管理单例

#import <Foundation/Foundation.h>

@interface BCManagerSingleton : NSObject

/**
 单例方法

 @return 返回单例对象
 */
+(instancetype)shareBCManagerSingleton;
@end
