//
//  BCManagerSingleton.m
//  RecordAndPlayVoice
//
//  Created by BigCat on 2017/6/23.
//  Copyright © 2017年 liuchunlao. All rights reserved.
//

#import "BCManagerSingleton.h"

@implementation BCManagerSingleton

// 静态变量
static BCManagerSingleton *_instance = nil;

// 启动的时候创建对象(这个方法会在程序每次启动的时候，把类加载到内存中)
+(void)load
{
    _instance = [[self alloc]init];
}

+(instancetype)shareBCManagerSingleton
{
    return _instance;
}

// 重写alloc方法
+(instancetype)alloc
{
    // 判断_instance是否已经存在
    if (_instance) {
        
        // 如果存在抛出异常
        // 创建异常
        NSException *singException = [NSException exceptionWithName:@"NSInternalInconsistencyException" reason:@"There can only be one Singleton instance" userInfo:nil];
        
        // 抛出异常
        [singException raise];
        
    }
    return  [super alloc];
}

@end
