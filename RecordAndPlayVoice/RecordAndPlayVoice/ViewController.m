//
//  ViewController.m
//  RecordAndPlayVoice
//
//  Created by PBOC CS on 15/3/12.
//  Copyright (c) 2015年 liuchunlao. All rights reserved.
//

#import "ViewController.h"
#import "LVRecordView.h"

#import "RCMediaViewController.h"
#import "RCMediaViewController+Handle.h"

#import "LocationDemoViewController.h"


@interface ViewController ()<RCMediaViewControllerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (nonatomic, strong) LVRecordView *recordView;
- (IBAction)baiduMap:(id)sender;

@end

@implementation ViewController
- (IBAction)paishe:(id)sender {
    RCMediaViewController *rvc = [[RCMediaViewController alloc] init];
    rvc.mediaDelegate = self;
    [self.navigationController pushViewController:rvc animated:YES];
   
}
- (IBAction)luyin:(id)sender { self.recordView = [LVRecordView recordView];
    self.recordView.backgroundColor = [UIColor lightGrayColor];
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    self.recordView.frame = CGRectMake(50, 100, width - 2 * 50, 120);
    [self.view addSubview:self.recordView];
}

- (void)viewDidLoad {
    [super viewDidLoad];

}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.recordView removeFromSuperview];
}

#pragma mark---拍照代理
- (void)rc_mediaController:(RCMediaViewController *)media didFinishPickingMediaWithInfo:(NSDictionary<NSString *, NSObject *> *)info
{
    
    NSLog(@"%@",info);
    
    self.imageView.image = (UIImage *)info[RCMediaImageInfo];
}

- (void)rc_mediaControlelrDidCancel:(RCMediaViewController *)media
{
    
}
- (IBAction)baiduMap:(id)sender {
    
    LocationDemoViewController *location = [[LocationDemoViewController alloc]init];
    [self.navigationController pushViewController:location animated:YES];
}
@end

